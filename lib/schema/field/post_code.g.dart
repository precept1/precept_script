// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_code.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FPostCode _$FPostCodeFromJson(Map<String, dynamic> json) => FPostCode(
      defaultValue: json['defaultValue'] == null
          ? null
          : PostCode.fromJson(json['defaultValue'] as Map<String, dynamic>),
      required: json['required'] as bool? ?? false,
      validation: json['validation'] as String?,
    );

Map<String, dynamic> _$FPostCodeToJson(FPostCode instance) {
  final val = <String, dynamic>{
    'validation': instance.validation,
    'required': instance.required,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('defaultValue', instance.defaultValue?.toJson());
  return val;
}
